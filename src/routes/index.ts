import { Router } from 'express';
import hello from './hello';

const routes = Router();
routes.use(hello);

export default routes;
