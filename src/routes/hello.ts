import { Router } from 'express';

const hello = Router();

hello.get('/hello', async (req, res) => {
  res.send('Hello world, welcome to the universe');
});

export default hello;
