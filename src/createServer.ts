import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { json, urlencoded } from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import resolvers from './resolvers';
import routes from './routes';

function context() {
  return {};
}

export default async function createServer() {
  const app = express();
  app.use(cors({
    origin: [/\/\/localhost/, process.env.APP_URL],
  }));

  app.use(json());
  app.use(urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use('/api', routes);

  const schema = await buildSchema({ resolvers });
  const apollo = new ApolloServer({
    schema,
    context,
  });

  apollo.applyMiddleware({ app });

  return { app, apollo };
}
