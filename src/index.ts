import 'reflect-metadata';
import { createPrismaClient } from './database';
import createServer from './createServer';

const port = Number(process.env.PORT || 3001);

async function main() {
  console.log('Create Prisma client');
  await createPrismaClient();

  console.log('Create server');
  const { apollo, app } = await createServer();

  console.log('Starting');
  app.listen({ port }, () => {
    console.log(`Graphql ready http://localhost:${port}${apollo.graphqlPath}`);
    console.log(`Rest api ready http://localhost:${port}/api`);
  });
}

main().catch(console.error);
