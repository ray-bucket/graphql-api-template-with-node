import { Field, InputType } from 'type-graphql';

@InputType()
export default class TodoInput {
  @Field(() => String)
  name: string;

  @Field(() => Boolean)
  done: boolean;
}
