import { Field, Int, ObjectType } from 'type-graphql';

@ObjectType()
export default class TodoType {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  name: string;

  @Field(() => Boolean)
  done: boolean;
}
