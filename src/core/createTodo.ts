import { Todo } from '.prisma/client';
import { getPrismaClient } from '../database';

export default async function createTodo(todo: Omit<Todo, 'id'>) {
  const prisma = getPrismaClient();
  const newTodo = await prisma.todo.create({ data: todo });

  return newTodo;
}
