import { getPrismaClient } from '../database';

export default async function getTodo(id: number) {
  const prisma = getPrismaClient();
  const todo = await prisma.todo.findFirst({
    where: { id },
  });

  return todo;
}
