import { getPrismaClient } from '../database';

export default async function getTodos() {
  const prisma = getPrismaClient();
  const todos = await prisma.todo.findMany();

  return todos;
}
