import { PrismaClient } from '@prisma/client';

let prisma: PrismaClient = null;

export async function createPrismaClient() {
  prisma = new PrismaClient();
  await prisma.$connect();

  return prisma;
}

export function getPrismaClient() {
  return prisma;
}
