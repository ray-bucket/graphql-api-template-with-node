import {
 Arg, Int, Mutation, Query, Resolver,
} from 'type-graphql';
import createTodo from '../core/createTodo';
import getTodo from '../core/getTodo';
import getTodos from '../core/getTodos';
import TodoInput from '../inputs/TodoInput';
import TodoType from '../types/TodoType';

@Resolver()
export default class TodoResolver {
  @Query(() => TodoType)
  async getTodo(
    @Arg('input', () => Int)
    input: number,
  ): Promise<TodoType> {
    const todo = await getTodo(input);

    return todo;
  }

  @Query(() => [TodoType])
  async getTodos(): Promise<TodoType[]> {
    const todos = await getTodos();

    return todos;
  }

  @Mutation(() => TodoType)
  async createTodo(
    @Arg('input', () => TodoInput)
    input: TodoInput,
  ): Promise<TodoType> {
    const todo = createTodo(input);

    return todo;
  }

  @Mutation(() => [TodoType])
  async createTodos(
    @Arg('input', () => [TodoInput])
    input: TodoInput[],
  ): Promise<TodoInput[]> {
    const promises = input.map((todoData) => createTodo(todoData));
    const todos = Promise.all(promises);

    return todos;
  }
}
