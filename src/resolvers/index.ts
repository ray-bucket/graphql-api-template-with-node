import { NonEmptyArray } from 'type-graphql';
import HelloResolver from './HelloResolver';
import TodoResolver from './TodoResolver';

const resolvers: NonEmptyArray<Function> = [HelloResolver, TodoResolver];

export default resolvers;
