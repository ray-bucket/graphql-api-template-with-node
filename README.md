# Graphql API template with node
This template includes
- Typescript
- eslint + airbnb
- jest + ts-jest
- prisma ORM
- debug for vscode
- basic docker-compose.yaml for development
- graphql api with apollo server + type-graphql
- rest api

## Configuration
- Install docker https://docs.docker.com/engine/install/
- create `prisma/.env` file and add the following content
```
DATABASE_URL = "postgresql://my-user:my-password@localhost:5432/my-db?schema=public"
```
you can set anothers credentials on docker-compose.yaml and 

## Run project
```
degit https://gitlab.com/ray-bucket/graphql-api-template-with-node my-project
cd my-project
npx prisma generate
npx prisma migrate dev
yarn
docker-compose up -d
yarn dev
```
